import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachineGUI {
    private JPanel root;
    private JTextPane ordersList;
    private JButton checkOutButton;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton paelyaButton;
    private JButton gyouzaButton;
    private JButton onigiriButton;
    private JLabel totalText;
    private JLabel total;
    private JButton cancelTempuraButton;
    private JButton cancelRamenButton;
    private JButton cancelUdonButton;
    private JButton cancelPaelyaButton;
    private JButton cancelGyouzaButton;
    private JButton cancelOnigiriButton;
    private int ramenPrice = 300;
    private int tempuraPrice = 500;
    private int udonPrice = 150;
    private int paelyaPrice = 450;
    private int gyouzaPrice = 200;
    private int onigiriPrice = 100;
    private int allPrice = 0;
    private int Tempuracnt = 0;
    private int Udoncnt = 0;
    private int Gyouzacnt = 0;
    private int Ramencnt = 0;
    private int Paelyacnt = 0;
    private int Onigiricnt = 0;
    void order(String food){
        JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
    }
    void cancel(String food){
        JOptionPane.showMessageDialog(null,food + " has been canceled.");
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public FoodOrderingMachineGUI() {

        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tempura.jpg")
        ));
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen.jpg")
        ));
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("Udon.jpg")
        ));
        paelyaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Paelya.jpg")
        ));
        gyouzaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Gyouza.jpg")
        ));
        onigiriButton.setIcon(new ImageIcon(
                this.getClass().getResource("Onigiri.jpg")
        ));

        String defaultText = ordersList.getText();
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Tempura?\nPrice: 500 yen", "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    order("Tempura");
                    allPrice += tempuraPrice;
                    total.setText(allPrice + " yen");
                    String currentText = ordersList.getText();
                    ordersList.setText(currentText + "Tempura: 500 yen" + "\n");
                    Tempuracnt++;
                }
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Ramen?\nPrice: 300 yen", "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    order("Ramen");
                    allPrice += ramenPrice;
                    total.setText(allPrice + " yen");
                    String currentText = ordersList.getText();
                    ordersList.setText(currentText + "Ramen: 300 yen" + "\n");
                    Ramencnt++;
                }
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Udon?\nPrice: 150 yen", "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    order("Udon");
                    allPrice += udonPrice;
                    total.setText(allPrice + " yen");
                    String currentText = ordersList.getText();
                    ordersList.setText(currentText + "Udon: 150 yen" + "\n");
                    Udoncnt++;
                }
            }
        });
        paelyaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Paelya?\nPrice: 450 yen", "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    order("Paelya");
                    allPrice += paelyaPrice;
                    total.setText(allPrice + " yen");
                    String currentText = ordersList.getText();
                    ordersList.setText(currentText + "Paelya: 450 yen" + "\n");
                    Paelyacnt++;
                }
            }
        });
        gyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Gyouza?\nPrice: 200 yen", "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    order("Gyouza");
                    allPrice += gyouzaPrice;
                    total.setText(allPrice + " yen");
                    String currentText = ordersList.getText();
                    ordersList.setText(currentText + "Gyouza: 200 yen" + "\n");
                    Gyouzacnt++;
                }
            }
        });
        onigiriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Onigiri?\nPrice: 100 yen", "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    order("Onigiri");
                    allPrice += onigiriPrice;
                    total.setText(allPrice + " yen");
                    String currentText = ordersList.getText();
                    ordersList.setText(currentText + "Onigiri: 100yen" + "\n");
                    Onigiricnt++;
                }
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?", "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    if (allPrice > 0) {
                        JOptionPane.showMessageDialog(null,"Thank you. The total price is " + allPrice + " yen.");
                        allPrice = 0;
                        total.setText(allPrice + " yen");
                        ordersList.setText(defaultText);
                        Tempuracnt = 0;
                        Ramencnt = 0;
                        Gyouzacnt = 0;
                        Paelyacnt = 0;
                        Onigiricnt = 0;
                        Udoncnt = 0;
                    }
                    else if(allPrice <= 0){
                        JOptionPane.showMessageDialog(null,"Please order something.");
                    }
                }
            }
        });
        cancelTempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel Tempura?", "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    if(Tempuracnt > 0){
                        Tempuracnt--;
                        cancel("Tempura");
                        allPrice -= tempuraPrice;
                        total.setText(allPrice + " yen");
                        String currentText = ordersList.getText();
                        ordersList.setText(currentText + "Cancel Tempura: -500 yen" + "\n");
                    }
                    else if(Tempuracnt <= 0){
                        JOptionPane.showMessageDialog(null,"Not ordered");
                    }
                }
            }
        });
        cancelRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel Ramen?", "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    if(Ramencnt > 0){
                        Ramencnt--;
                        cancel("Ramen");
                        allPrice -= ramenPrice;
                        total.setText(allPrice + " yen");
                        String currentText = ordersList.getText();
                        ordersList.setText(currentText + "Cancel Ramen: -300 yen" + "\n");
                    }
                    else if(Ramencnt <= 0){
                        JOptionPane.showMessageDialog(null,"Not ordered");
                    }
                }
            }
        });
        cancelUdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel Udon?", "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    if(Udoncnt > 0){
                        Udoncnt--;
                        cancel("Udon");
                        allPrice -= udonPrice;
                        total.setText(allPrice + " yen");
                        String currentText = ordersList.getText();
                        ordersList.setText(currentText + "Cancel Udon: -150 yen" + "\n");
                    }
                    else if(Udoncnt <= 0){
                        JOptionPane.showMessageDialog(null,"Not ordered");
                    }
                }
            }
        });
        cancelPaelyaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel Paelya?", "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    if(Paelyacnt > 0){
                        Paelyacnt--;
                        cancel("Paelya");
                        allPrice -= paelyaPrice;
                        total.setText(allPrice + " yen");
                        String currentText = ordersList.getText();
                        ordersList.setText(currentText + "Cancel Paelya: -450 yen" + "\n");
                    }
                    else if(Paelyacnt <= 0){
                        JOptionPane.showMessageDialog(null,"Not ordered");
                    }
                }
            }
        });
        cancelGyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel Gyouza?", "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    if(Gyouzacnt > 0){
                        Gyouzacnt--;
                        cancel("Gyouza");
                        allPrice -= gyouzaPrice;
                        total.setText(allPrice + " yen");
                        String currentText = ordersList.getText();
                        ordersList.setText(currentText + "Cancel Gyouza: -200 yen" + "\n");
                    }
                    else if(Gyouzacnt <= 0){
                        JOptionPane.showMessageDialog(null,"Not ordered");
                    }
                }
            }
        });
        cancelOnigiriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel Onigiri?", "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    if(Onigiricnt > 0){
                        Onigiricnt--;
                        cancel("Onigiri");
                        allPrice -= onigiriPrice;
                        total.setText(allPrice + " yen");
                        String currentText = ordersList.getText();
                        ordersList.setText(currentText + "Cancel Onigiri: -100 yen" + "\n");
                    }
                    else if(Onigiricnt <= 0){
                        JOptionPane.showMessageDialog(null,"Not ordered");
                    }
                }
            }
        });
    }
}